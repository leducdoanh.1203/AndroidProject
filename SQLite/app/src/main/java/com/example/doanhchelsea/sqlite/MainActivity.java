package com.example.doanhchelsea.sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.doanhchelsea.sqlite.adapter.CustomAdapter;
import com.example.doanhchelsea.sqlite.data.DBManager;
import com.example.doanhchelsea.sqlite.model.Student;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText edtName;
    private EditText edtAddress;
    private EditText edtPhoneNumber;
    private EditText edtEmail;
    private EditText edtId;
    private Button btnSave;
    private Button btnUpdate;
    private ListView lvStudent;
    private DBManager dbManager;
    private CustomAdapter customAdapter;
    private List<Student> studentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final DBManager dbManager = new DBManager(this);
        intWidget();

        studentList = dbManager.getAllStudent();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student student = createStudent();
                if(student!= null){
                dbManager.addStudent(student);
                }
                studentList.clear();
                studentList.addAll(dbManager.getAllStudent());
                setAdapter();
            }
        });
        lvStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Student student = studentList.get(position);
                edtId.setText(String.valueOf(student.getmId()));
                edtName.setText(student.getmName());
                edtAddress.setText(student.getmAddress());
                edtEmail.setText(student.getmEmail());
                edtPhoneNumber.setText(student.getmNumber());
                btnSave.setEnabled(false);
                btnUpdate.setEnabled(true);
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student student = new Student();
                student.setmId(Integer.parseInt(String.valueOf(edtId.getText())));
                student.setmName(edtName.getText()+"");
                student.setmAddress(edtAddress.getText()+"");
                student.setmEmail(edtEmail.getText()+"");
                student.setmNumber(edtPhoneNumber.getText()+"");
                int result =  dbManager.updateStudent(student);
                if(result>0){
                    updateListStudent();
                }else {
                    btnSave.setEnabled(true);
                    btnUpdate.setEnabled(false);
                }
            }
        });
        lvStudent.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Student student = studentList.get(position);
                int result = dbManager.deleteStudent(student.getmId());
                if(result>0){
                    Toast.makeText(MainActivity.this, "Đã xóa thành công ", Toast.LENGTH_SHORT).show();
                    updateListStudent();
                }
                else {
                    Toast.makeText(MainActivity.this, "Xóa lỗi ", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }
    private Student createStudent(){
        String name = edtName.getText().toString();
        String address = String.valueOf(edtAddress.getText());
        String phoneNumber = edtPhoneNumber.getText().toString();
        String email = edtEmail.getText().toString();
        Student student = new Student(name,address,phoneNumber,email);

        return student;
    }
    private void intWidget(){
        edtName = findViewById(R.id.edt_name);
        edtAddress = findViewById(R.id.edt_name);
        edtPhoneNumber = findViewById(R.id.edt_number);
        edtEmail = findViewById(R.id.edt_email);
        btnSave = findViewById(R.id.btn_save);
        lvStudent = findViewById(R.id.lv_student);
        edtId =findViewById(R.id.edt_id);
        btnUpdate = findViewById(R.id.btn_update);
    }
    private void setAdapter(){
        if(customAdapter ==null){
            customAdapter = new CustomAdapter(this,R.layout.item_list_student,studentList);
            lvStudent.setAdapter(customAdapter);
        }else {
            customAdapter.notifyDataSetChanged();
            lvStudent.setSelection(customAdapter.getCount()-1);
        }

    }
    public void updateListStudent(){
        studentList.clear();
        studentList.addAll(dbManager.getAllStudent());
        if (customAdapter!=null) {
            customAdapter.notifyDataSetChanged();

        }

    }
}
