package com.example.doanhchelsea.sqlite.model;

/**
 * Created by doanhchelsea on 30/01/2018.
 */

public class Student {
    private int mId;
    private String mName;
    private String mNumber;
    private String mEmail;
    private String mAddress;

    public Student() {

    }

    public Student(String mName, String mNumber, String mEmail, String mAddress) {
        this.mName = mName;
        this.mNumber = mNumber;
        this.mEmail = mEmail;
        this.mAddress = mAddress;
    }

    public Student(int mId, String mName, String mNumber, String mEmail, String mAddress) {
        this.mId = mId;
        this.mName = mName;
        this.mNumber = mNumber;
        this.mEmail = mEmail;
        this.mAddress = mAddress;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmNumber() {
        return mNumber;
    }

    public void setmNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    @Override
    public String toString() {
        return "Student{" +
                "mName='" + mName + '\'' +
                ", mNumber='" + mNumber + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mAddress='" + mAddress + '\'' +
                '}';
    }
}
