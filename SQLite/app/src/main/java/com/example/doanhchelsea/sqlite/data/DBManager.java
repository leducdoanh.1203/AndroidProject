package com.example.doanhchelsea.sqlite.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.doanhchelsea.sqlite.model.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by doanhchelsea on 30/01/2018.
 */

public class DBManager extends SQLiteOpenHelper {
    public static final String DATABASE_NAME ="student_list";
    private static final String TABLE_NAME ="student";
    private static final String ID ="id";
    private static final String NAME ="name";
    private static final String EMAIL ="email";
    private static final String NUMBER ="number";
    private static final String ADDRESS ="address";
    private static int VERSION =1;
    private Context context;


    public DBManager(Context context) {
        super(context, DATABASE_NAME, null,VERSION);
        this.context= context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String SQLQuery = "CREATE TABLE "+TABLE_NAME+" ("+
                ID +" integer primary key, "+
                NAME + " TEXT, "+
                EMAIL +" TEXT, "+
                NUMBER+" TEXT," +
                ADDRESS +" TEXT)";
        sqLiteDatabase.execSQL(SQLQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    public void addStudent(Student student){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME,student.getmName());
        values.put(NUMBER,student.getmNumber());
        values.put(EMAIL,student.getmEmail() );
        values.put(ADDRESS,student.getmAddress());
        db.insert(TABLE_NAME,null,values);
        db.close();
    }
    public List<Student> getAllStudent(){
        List<Student> listStudent = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do{
                Student student = new Student();
                student.setmId(cursor.getInt(0));
                student.setmName(cursor.getString(1)+"");
                student.setmEmail(cursor.getString(2));
                student.setmNumber(cursor.getString(3));
                student.setmAddress(cursor.getString(4));
                listStudent.add(student);
            }while (cursor.moveToNext());
        }
        db.close();
        return listStudent;
    }
    public int updateStudent(Student student){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,student.getmName());
        contentValues.put(ADDRESS,student.getmAddress());
        contentValues.put(EMAIL,student.getmEmail());
        contentValues.put(NUMBER,student.getmNumber());
        db.close();
        return db.update(TABLE_NAME,contentValues,ID+"?",new String[]{String.valueOf(student)});
    }
    public int deleteStudent(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
        return db.delete(TABLE_NAME,ID+"=?",new String[]{String.valueOf(id)});

    }
}
