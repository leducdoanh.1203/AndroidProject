package com.example.doanhchelsea.demo;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.doanhchelsea.demo.adapter.CustomAdapter;
import com.example.doanhchelsea.demo.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView lvContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvContact = findViewById(R.id.lv_contact);

        ArrayList<Contact> arrayList = new ArrayList<>();

        Contact contact1 = new Contact(Color.BLUE,"Lê Đức Doanh ","0967799614");
        Contact contact2 = new Contact(Color.RED,"Trần Hải Đăng  ","01234567890");
        Contact contact3 = new Contact(Color.YELLOW,"Hoàng Văn Đạt  ","09876543210");
        Contact contact4 = new Contact(Color.GREEN,"Hoàng Văn Tú  ","0967799614");
        Contact contact5 = new Contact(Color.BLACK,"Nguyễn Sơn Tùng  ","0967799614");
        arrayList.add(contact1);
        arrayList.add(contact2);
        arrayList.add(contact3);
        arrayList.add(contact4);
        arrayList.add(contact5);

        CustomAdapter customAdapter = new CustomAdapter(this,R.layout.row_item_contact,arrayList);
        lvContact.setAdapter(customAdapter);
    }

}
