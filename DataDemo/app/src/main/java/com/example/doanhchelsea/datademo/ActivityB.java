package com.example.doanhchelsea.datademo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityB extends AppCompatActivity {
    private TextView tvTitle,tvDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        tvTitle = findViewById(R.id.tv_title);
        tvDescription = findViewById(R.id.tv_description);
//        setDataExtras();
        setBundel();
    }
    public void setDataExtras(){
        Intent intent = getIntent();
        String title = intent.getStringExtra(ActivityA.TITLE);
        String description = intent.getStringExtra(ActivityA.DESCRIPTION);
        tvTitle.setText(title);
        tvDescription.setText(description);
    }
    public void setBundel(){
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra(ActivityA.BUNDLE);
        String title = bundle.getString(ActivityA.TITLE);
        String description = bundle.getString(ActivityA.DESCRIPTION);
        tvTitle.setText(title);
        tvDescription.setText(description);
    }


}
