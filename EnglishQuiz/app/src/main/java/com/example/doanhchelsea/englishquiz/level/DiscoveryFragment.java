package com.example.doanhchelsea.englishquiz.level;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.doanhchelsea.englishquiz.MainActivity;
import com.example.doanhchelsea.englishquiz.R;
import com.example.doanhchelsea.englishquiz.slide.ScreenSlideActivity;
import com.example.doanhchelsea.englishquiz.slide.ScreenSlidePageFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoveryFragment extends Fragment {
    TopicDiscoveryAdapter topicDiscoveryAdapter;
    ListView lvDiscovery;
    ArrayList<TopicDiscovery> arr_topicDis = new ArrayList<TopicDiscovery>();

    public DiscoveryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Discovery the world");
        return inflater.inflate(R.layout.fragment_discovery, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        lvDiscovery = getActivity().findViewById(R.id.lvDicovery);
        arr_topicDis.add(new TopicDiscovery("Arkansas"));
        arr_topicDis.add(new TopicDiscovery("Australia"));
        arr_topicDis.add(new TopicDiscovery("Croatia"));
        arr_topicDis.add(new TopicDiscovery("England"));
        topicDiscoveryAdapter = new TopicDiscoveryAdapter(getActivity(),arr_topicDis);
        lvDiscovery.setAdapter(topicDiscoveryAdapter);
        lvDiscovery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(),ScreenSlideActivity.class);
                startActivity(intent);
            }
        });


    }
}
