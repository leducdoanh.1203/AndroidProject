package com.example.doanhchelsea.englishquiz.level;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.doanhchelsea.englishquiz.MainActivity;
import com.example.doanhchelsea.englishquiz.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Level1Fragment extends Fragment {


    public Level1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Level 1");
        return inflater.inflate(R.layout.fragment_level1, container, false);
    }

}
