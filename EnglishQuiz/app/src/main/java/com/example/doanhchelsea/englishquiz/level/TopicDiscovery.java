package com.example.doanhchelsea.englishquiz.level;

/**
 * Created by doanhchelsea on 28/02/2018.
 */

public class TopicDiscovery {
    private String name;

    public TopicDiscovery(String name) {
        this.name = name;
    }

    public TopicDiscovery() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
