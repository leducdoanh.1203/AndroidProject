package com.example.doanhchelsea.englishquiz.level;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.doanhchelsea.englishquiz.R;

import java.util.ArrayList;

/**
 * Created by doanhchelsea on 28/02/2018.
 */

public class TopicDiscoveryAdapter extends ArrayAdapter<TopicDiscovery> {
    public TopicDiscoveryAdapter(@NonNull Context context, ArrayList<TopicDiscovery> topicDiscoveries) {
        super(context,0,topicDiscoveries);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_listview,parent,false);
        }
        TextView tvName = convertView.findViewById(R.id.tvNumExam);
        TopicDiscovery p = getItem(position);
        if (p!=null){
            tvName.setText(""+p.getName());
        }

        return convertView;
    }
}
