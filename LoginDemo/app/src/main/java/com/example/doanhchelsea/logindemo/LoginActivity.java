package com.example.doanhchelsea.logindemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private Button btnLogin,btnRegister;
    private EditText edtUser,edtPassword;
    private String inputA,inputB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);
        edtUser = findViewById(R.id.edt_user);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputA = String.valueOf(edtUser.getText());
                inputB = String.valueOf(edtPassword.getText());
                Toast.makeText(LoginActivity.this, "Login with username: "+inputA+" and "+" password: "+inputB, Toast.LENGTH_SHORT).show();
            }
        });
       btnRegister.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(LoginActivity.this,MainActivity.class);
               startActivity(intent);
           }
       });
    }
}
